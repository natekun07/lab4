mixin commonValidation{
  String? validateEmail(String? value) {
    if(!value!.contains('@')) {
      return "You input wrong type of email";
    }
    if(!value .contains('.')){
      return "You missing '.' character";
    }
    else {
      return null;
    }
  }

  String? validatePassword(String? value) {
    if(value!.length < 8) {
      return "Password need more than 8 characters";
    }
    else {
      return null;
    }
  }
}