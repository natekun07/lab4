import 'package:flutter/material.dart';
import 'package:tiki_demo_program/screen/FoodScreen.dart';
import 'package:tiki_demo_program/screen/HomeScreen.dart';
import '../screen/LoginScreen.dart';
import '../screen/News.dart';
import '../screen/fashionScreen.dart';

class myApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyStatefulWidget(),
      );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  final screens = [
    HomeSreen(),
    News(),
    LoginScreen(),
  ];


  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            ListTile(
              title: const Text('Food'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute<void>(
                  builder: (BuildContext context) {
                    return Scaffold(
                      appBar: AppBar(title: Text('Tiki')),
                      body: foodScreen()
                    );
                  },
                ));
              },
            ),
            ListTile(
              title: const Text('fashion'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute<void>(
                  builder: (BuildContext context) {
                    return Scaffold(
                        appBar: AppBar(title: Text('Tiki')),
                        body: fashionScreen()
                    );
                  },
                ));
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Tiki'),
      ),
      body: screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blue,
        type: BottomNavigationBarType.fixed,
        showUnselectedLabels: false,

        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.airplay),
            label: 'News',
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.login),
            label: 'Login',
          ),

        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color.fromARGB(255, 237, 239, 243),
        onTap: _onItemTapped,
      ),
    );
  }
}
