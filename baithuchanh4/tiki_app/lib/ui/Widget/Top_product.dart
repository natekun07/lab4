import 'package:flutter/material.dart';
import 'package:tiki_demo_program/ui/Widget/product_card.dart';

import '../../Constrants/constants.dart';
import 'Product.dart';

class Firstlist extends StatefulWidget {
  @override
  _FirstlistState createState() => _FirstlistState();
}

class _FirstlistState extends State<Firstlist> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      child: Card(
        child: Row(
          children: List.generate(demo_product.length, (index) => Padding(padding: EdgeInsets.symmetric(vertical: defaultPadding),
            child: ProductCard(
              title: demo_product[index].title,
              price: demo_product[index].price,
              bgColor: demo_product[index].bgColor,
              image: demo_product[index].image,
              press: () {
                Navigator.push(context, MaterialPageRoute<void>(
                  builder: (BuildContext context) {
                    return Scaffold(
                      appBar: AppBar(title: Text('Tiki')),
                      body: Center(
                        child: FlatButton(
                          child: Text('Product detail'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    );
                  },
                ));
              },
            ),)),
        ),
      ),
    );
  }
}
